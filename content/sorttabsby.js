// This file is part of SortTabsBy.

// SortTabsBy is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// SortTabsBy is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with SortTabsBy.  If not, see <http://www.gnu.org/licenses/>.

var SortTabsBy = (function () {
    "use strict";

    let CC = Components.classes;
    let CI = Components.interfaces;

    let Prefs = CC["@mozilla.org/preferences-service;1"]
	.getService(CI.nsIPrefService).getBranch("extensions.sorttabsby.");

    let o = {};

    o.sort = function () {
	let tabs = [];
	let how = Prefs.getCharPref("sortby");

	for (let i=0; i<gBrowser.tabs.length; ++i) {
	    tabs[i] = gBrowser.tabs[i];
	}

	let sorted = how === 'title' ?
	    tabs.sort(function (a, b) {
		return a.label < b.label ? -1
		    : a.label > b.label ? 1
		    : 0;
	    })
	    : tabs.sort(function (a, b) {
		let c = a.linkedBrowser.currentURI.spec;
		let d = b.linkedBrowser.currentURI.spec;

		return c < d ? -1 : c > d ? 1 : 0;
	    });

	sorted.forEach(gBrowser.moveTabTo, gBrowser);
    };

    return o;
})();
