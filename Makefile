# This file is part of SortTabsBy.

# SortTabsBy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# SortTabsBy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with SortTabsBy.  If not, see <http://www.gnu.org/licenses/>.

CONTENT=install.rdf chrome.manifest options.xul content/* locale/* defaults/* \
	icon.png icon64.png

all: sorttabsby.xpi

sorttabsby.xpi: $(CONTENT)
	zip -r $@ $^
